# Maricio Hernández Siles
# A01700303
# 17/05/2020
# ------
# Analizador Léxico & Sintáctico
# Método de ejecución: python3 pascalCompiler.py example.txt grammar.txt actions_table.txt gotos_table.txt
# ------
# Consideraciones:
# La gramática al estar incompleta no llega al estado de aceptación y no logra reconocer todos los tokens definidos.
# Cuando se vacía el input ($) se toma como reconocido y correcto.
# ------

import sys
import tokenize

#Definir las palabras reservadas del lenguaje de programación
reserved = [
    "program",
    "begin",
    "end",
    "function",
    "constant",
    "if",
    "else",
    "then",
    "var",
    "const"
    "do",
    "while",
    "repeat",
    "for",
    "until",
    "to",
    "downto",
    "writeln",
    "readln"
]

types = [
    "integer",
    "string",
    "boolean",
    "real"
]

operators = [
    "and",
    "or",
    "not",
    "mod",
    "div"
]

dict = {
    "system": 1,
    "number": 2,
    "string": 3,
    "operator": 53
}

#Pertenece a un grupo de caracteres específico
def belongs(token, keyList):
    if token in keyList:
        return True
    return False

#Imprimir los tokens
def printTokens(tokens):
    for token in tokens:
        print(token)

#Para los tokens reservados del sistema
def addSystemToken(tokenList, tokenString):
    if belongs(tokenString, reserved):
        tokenList.append(tokenString)
    elif belongs(tokenString, operators):
        tokenList.append(tokenString)
    #Tipos de variables (Int, String, Bool, etc)
    elif belongs(tokenString, types):
        tokenList.append(tokenString)
    else:
        tokenList.append("identifier")

    return tokenList

#Obtener los tokens del código a compilar
def getTokens(filePath):
    with tokenize.open(filePath) as file:
        tokens = tokenize.generate_tokens(file.readline)
        tokenList = []
        for token in tokens:
            tokenType = token.type
            originalString = token.string
            tokenString = token.string.lower()
            if tokenType == dict.get('system'):
                tokenList = addSystemToken(tokenList, tokenString)

            elif tokenType ==  dict.get('number'):
                tokenList.append('number')

            elif tokenType ==  dict.get('string'):
                tokenList.append("string")

            elif tokenType ==  dict.get('operator'):
                if(tokenString == ":"):
                    #Get the next value of the generator
                    nextToken = next(tokens)
                    if(nextToken.string == "="):
                        #Los agrega como uno solo
                        tokenList.append(tokenString + nextToken.string.lower())
                    else:
                        #Agregar los token por separado
                        tokenList.append(tokenString)
                        if nextToken.type == dict.get('system'):
                            tokenList = addSystemToken(tokenList, nextToken.string.lower())
                        else:
                            tokenList.append(nextToken.string.lower())
                else:
                    tokenList.append(tokenString)

    return tokenList

#Obtener la tabla de los archivos .txt (gotoTable y actionsTable)
def getTable(fileTxt):
    originalFile = open(fileTxt, 'r')
    table = [line.split(',') for line in originalFile.readlines()]
    newTable = []
    for element in table:
        #Eliminar el '\n' de cada lista
        cleanList = list(map(str.strip,element))
        #To add commas missing
        if(cleanList[1] == '"'):
            cleanList = [cleanList[0], ',', cleanList[3], cleanList[4]]
        newTable.append(cleanList)

    return newTable

#Buscar una acción en la tabla
def getAction(stack, token, actionsTable):
    for action in actionsTable:
        if stack == int(action[0]):
            if token == action[1]:
                return action[2], int(action[3])
    return -1

#Buscar un goto en la tabla
def getGoTo(stack, symbol, goToTable):
    for goTo in goToTable:
        if stack == int(goTo[0]):
            if symbol == goTo[1]:
                return int(goTo[2])
    return -1


#Analizador Sintáctico
def parserSLR(input, actionsTable, goToTable, grammar):
    stack = []
    symbols = []
    stack.append(0)
    goToFlag = False

    #Mientras se reconozca una acción y no se haya terminado la ejecución
    while True:

        # print("STACK: ", stack)
        # print("SYMBOLS: ", symbols)
        # print("INPUT: ", input)

        #Si se ejecutó un reduce, se activa la bandera del goto
        if goToFlag:
            try:
                goToAction = getGoTo(stack[-1], symbols[-1], goToTable)
            except:
                print("---------------------")
                print("ERROR: No goto found")
                print("STACK: ", stack)
                print("SYMBOLS: ", symbols)
                print("INPUT: ", input)
                print("---------------------")
                break
            print("GOTO:", goToAction)
            stack.append(goToAction)
            goToFlag = False
        else:
            #Al no reconocer nunca el "ACC" cuando se vacía el input se toma como aceptado
            if input[0] == '$':
                print("---------------------")
                print("SUCCESS!")
                print("STACK: ", stack)
                print("SYMBOLS: ", symbols)
                print("INPUT: ", input)
                print("---------------------")
                break

            #Buscar la acción en la tabla
            try:
                #Get last value of stack, get first value of input
                action, number = getAction(stack[-1], input[0], actionsTable)
            except:
                print("---------------------")
                print("ERROR: No action found")
                print("STACK: ", stack)
                print("SYMBOLS: ", symbols)
                print("INPUT: ", input)
                print("---------------------")
                break

            #Cuando llega al estado de aceptación
            if action == 'ACCEPT':
                print("---------------------")
                print("SUCCESS!")
                print("STACK: ", stack)
                print("SYMBOLS: ", symbols)
                print("INPUT: ", input)
                print("---------------------")
                break
            #Si es shift
            elif action == 's':
                print("SHIFT:", number)
                stack.append(number)
                symbols.append(input[0])
                input.pop(0)
            #Si es reduce
            elif action == 'r':
                print("REDUCE:", number)
                action = grammar[number].split()
                # print("ACTION", action)
                for i in range(len(action)-2):
                    #Para saber si la producción NO es 'epsilon'
                    if action[i+2] != "''":
                        # print(action[i+2])
                        symbols.pop()
                        stack.pop()
                    else:
                        break
                symbols.append(action[0])
                goToFlag = True

#Funcion Main
def main():
    tokenList = getTokens(sys.argv[1])
    tokenList.append("$")

    #Cargar las tablas
    grammar = open(sys.argv[2], 'r').readlines()
    actionsTable = getTable(sys.argv[3])
    goToTable = getTable(sys.argv[4])

    parserSLR(tokenList, actionsTable, goToTable, grammar)


#Execute the program
main()
