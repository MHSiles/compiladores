from tag import Tag

def main():

    index = 0

    with open("Examples/Example1.pas") as myfile:
        file = myfile.read()

    while index < len(file):
        # print(file[index])
        if file[index] != ' ' or file[index] != '\t' or file[index] != '\n':
            if file[index] == '=' and file[index+1] == '=':
                    #REGRESAR EQUALS
                    # print(tags['EQ'])
                    index += 1
            elif file[index] == '!' and file[index+1] == '=':
                    #REGRESAR NOT EQUALS
                    # print(tags['NE'])
                    index += 1
            elif file[index] == '<' and file[index+1] == '=':
                    #REGRESAR LESS THAN OR EQUALS TO
                    # print(tags['LE'])
                    index += 1
            elif file[index] == '>' and file[index+1] == '=':
                    #REGRESAR GREATER THAN OR EQUALS TO
                    # print(tags['GE'])
                    index += 1

            if(file[index].isdigit()):
                number = 0
                while True:
                    number = (10 * number) + int(file[index])
                    index += 1
                    if(not file[index].isdigit()):
                        break
                print(number)

            if(file[index].isalpha()):
                print("CHAR")

        index += 1



main()